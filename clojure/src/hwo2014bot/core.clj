(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [aleph.tcp :refer [tcp-client]]
            [lamina.core :refer [enqueue wait-for-result wait-for-message close]]
            [gloss.core :refer [string]])
  (:gen-class))

(set! *warn-on-reflection* true)

(def ^:dynamic *last-result* nil)

(defn json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
   (try
     (wait-for-message channel)
     (catch Exception e
       (println (str "ERROR: " (.getMessage e)))
       (System/exit 1)))))

(defn create-message [message-type data]
  {:msgType message-type :data data})

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn car-by-color [data color]
  (first (filter #(= color (get-in % [:id :color])) data)))

(defn curved-piece? [{:keys [length]}]
  (nil? length))

(defn arc-length [{:keys [radius angle]} distance-from-center]
  (let [c (+ radius (if (< angle 0) distance-from-center (* distance-from-center -1)))
        a (Math/abs ^Integer angle)]
    (/ (* Math/PI c a) 180)))

(defn piece-length [piece distance-from-center]
  (if (curved-piece? piece)
    (arc-length piece distance-from-center)
    (:length piece)))

(defn remaining-length [piece in-piece-distance distance-from-center]
  (let [length (piece-length piece distance-from-center)]
    (- length in-piece-distance)))

(defn current-velocity [{:keys [last-piece-index last-in-piece-distance pieces] :as state}
                        distance-from-center piece-index in-piece-distance]
  (let [last-piece (get pieces last-piece-index)
        piece (get pieces piece-index)]
    (if (= piece-index last-piece-index)
      (- in-piece-distance last-in-piece-distance)
      (+ in-piece-distance (remaining-length last-piece last-in-piece-distance distance-from-center)))))

(defn track-length [pieces distance-from-center]
  (reduce (fn [acc piece]
            (+ acc (piece-length piece distance-from-center))) 0 pieces))


;;rotate function copied from https://groups.google.com/forum/#!topic/clojure/SjmevTjZPcQ
(defn rotate
  "Take a collection and left rotates it n steps. If n is negative, the collection is rotated right. Executes in O(n) time."
  [n coll]
  (let [c (count coll)]
    (take c (drop (mod n c) (cycle coll)))))

(defn find-switch-index-after-piece [pieces piece-index]
  (let [rot-pieces (rotate (+ 1 piece-index) pieces)
        next-switch (some #(if (:switch %) %) rot-pieces)]
      (:index next-switch)))

(defn add-angles [acc piece]
  (if (:angle piece)
    (+ acc (:angle piece))
    acc))

(defn angle-sum-of-pieces [pieces start-index end-index]
  (let [sub-pieces (if (< start-index end-index)
                        (subvec pieces start-index end-index)
                        (subvec pieces end-index start-index))]
    (reduce add-angles 0 sub-pieces)))

(defn plan-lane-switch [pieces old-piece-index piece-index]
  (when (> piece-index old-piece-index)
    (let [next-piece-index (rem (+ piece-index 1) (count pieces))
          next-piece (get pieces next-piece-index)]
      (when (:switch next-piece)
        (let [next-switch-index (find-switch-index-after-piece pieces next-piece-index)
              angle-sum (angle-sum-of-pieces pieces next-piece-index next-switch-index)]
          (if (> angle-sum 0)
            (create-message "switchLane" "Right")
            (create-message "switchLane" "Left")
        ))))))


(defmulti handle-message (fn [_ message] (:msgType message)))

(defmethod handle-message "join" [state message]
  (println "join")
  [state])

(defmethod handle-message "gameStart" [state message]
  (println "game-start")
  [state])

(defmethod handle-message "lapFinished" [state message]
  (println "lap-finished" (:total-length state))
  [(assoc state :total-length 0)])

(defmethod handle-message "finish" [state message]
  (println "finish")
  [state])

(defmethod handle-message "gameEnd" [state message]
  (println "game-end")
  [state])

(defmethod handle-message "tournamentEnd" [state message]
  (println "tournament-end")
  [state nil true])

(defmethod handle-message "yourCar" [state message]
  (let [color (get-in message [:data :color])]
    (println "your-car ::" color)
    [(assoc state :color color)]))

(defn add-index [pieces]
  (vec (map-indexed (fn [idx piece] (assoc piece :index idx)) pieces)))

(defn fix-distance [lane]
  (update-in lane [:distanceFromCenter] #(* % -1)))

(defmethod handle-message "gameInit" [state message]
  (let [track (get-in message [:data :race :track])
        pieces (add-index (:pieces track))
        lanes (:lanes track)
        new-state (assoc state :pieces pieces :lanes lanes)
        tl (track-length pieces -10)]
    (println "track-length" tl)
    (println "game-init" lanes pieces)
    [new-state]))

(defmethod handle-message "carPositions" [{:keys [color] :as state} message]
  (let [player-data (car-by-color (:data message) color)
        angle (:angle player-data)
        piece-position (:piecePosition player-data)
        lane (get-in piece-position [:lane :startLaneIndex])
        lane-config (get-in state [:lanes lane])
        distance-from-center (:distanceFromCenter lane-config)
        old-piece-index (:last-piece-index state)
        piece-index (:pieceIndex piece-position)
        in-piece-distance (:inPieceDistance piece-position)
        new-state0 (assoc state
                     :last-angle angle
                     :last-piece-index piece-index
                     :last-in-piece-distance in-piece-distance
                     :lane lane)
        delta (current-velocity state distance-from-center piece-index in-piece-distance)
        new-state1 (update-in new-state0 [:metrics :velocity] conj [(:gameTick message) delta])
        new-state (update-in new-state1 [:total-length] + delta)]
    (println (:gameTick message))
    (let [switch-message (plan-lane-switch (:pieces state) old-piece-index piece-index)]
      (if (nil? switch-message)
        [new-state (create-message "throttle" (:throttle new-state))]
        [new-state switch-message]))))



;; "turboDurationMilliseconds": 500.0,
;; "turboDurationTicks": 30,
;; "turboFactor": 3.0

(defmethod handle-message "turboAvailable" [state message]
  (let [turbo-config (:data message)]
    (println "turbo-available" turbo-config)
    ;; (create-message :turbo "GODSPEED")
    [state]))

(defmethod handle-message "crash" [state message]
  (println "crash")
  [state])

(defmethod handle-message "spawn" [state message]
  (println "spawn")
  [state])

(defmethod handle-message "dnf" [state message]
  (println "dnf")
  [state])

(defmethod handle-message "error" [state message]
  (println (str "ERROR: " (:data message)))
  [state])

(defmethod handle-message :default [state message]
  (println "default" message)
  [state])

(defn get-initial-state [channel]
  {:channel channel
   :last-tick 0
   :velocity 0
   :total-length 0
   :throttle 0.65
   :last-piece-index 0
   :last-in-piece-distance 0
   :current-lane 0
   :color nil
   :pieces []
   :lanes []
   :metrics {:velocity []}})

(defn start-game-loop [channel]
  (let [ping-reply (create-message "ping" "ping")]
    (loop [state (get-initial-state channel)]
      (let [message (read-message channel)
            [new-state reply0 should-stop] (handle-message state message)
            reply (if (nil? reply0) ping-reply reply0)]
        (if (nil? should-stop)
          (do
            (send-message channel reply)
            (recur new-state))
          new-state)))))

(defn start [host port botname botkey]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel (create-message "join" {:name botname :key botkey}))
    (let [result (start-game-loop channel)]
      (alter-var-root #'*last-result* (constantly result))
      nil)))

(defn -main [& [host port botname botkey]]
  (start host port botname botkey))
