(ns hwo2014bot.metrics
  (:require [incanter.core :refer [with-data dataset view]]
            [incanter.charts :refer [xy-plot]]))

(defn get-metric [state metric-type]
  (get-in state [:metrics metric-type]))

(defn plot [velocity-data]
  (with-data (dataset [:tick :velocity] velocity-data)
    (view (xy-plot :tick :velocity))))

;; (plot (get-metric *last-result* :velocity))
